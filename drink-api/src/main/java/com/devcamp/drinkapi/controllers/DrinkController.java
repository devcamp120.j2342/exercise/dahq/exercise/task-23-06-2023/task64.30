package com.devcamp.drinkapi.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.drinkapi.models.Drink;
import com.devcamp.drinkapi.repositorys.DrinkRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class DrinkController {
    @Autowired
    DrinkRepository drinkRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<Drink>> getAllDrink() {
        try {
            List<Drink> drinks = new ArrayList<Drink>();
            drinkRepository.findAll().forEach(drinks::add);
            return new ResponseEntity<>(drinks, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/drinks/{id}")
    public ResponseEntity<Drink> getDrinkById(@PathVariable("id") long id) {
        try {
            Optional<Drink> cDrink = drinkRepository.findById(id);
            return new ResponseEntity<>(cDrink.get(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/drinks")
    public ResponseEntity<Drink> createDrink(@Valid @RequestBody Drink pDrink) {
        try {

            pDrink.setNgayTao(new Date());
            pDrink.setNgayCapNhat(null);
            Drink cDrink = drinkRepository.save(pDrink);
            return new ResponseEntity<>(cDrink, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/drinks/{id}")
    public ResponseEntity<Drink> upDateDrinkById(@PathVariable("id") long id, @RequestBody Drink pDrink) {
        try {
            Optional<Drink> cDrink = drinkRepository.findById(id);
            cDrink.get().setMaNuocUong(pDrink.getMaNuocUong());
            cDrink.get().setTenNuocUong(pDrink.getTenNuocUong());
            cDrink.get().setNgayCapNhat(pDrink.getNgayCapNhat());
            cDrink.get().setDonGia(pDrink.getDonGia());
            return new ResponseEntity<>(drinkRepository.save(cDrink.get()), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/drinks/{id}")
    public ResponseEntity<Drink> deleteDrinkById(@PathVariable("id") long id) {
        try {
            drinkRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/drinks")
    public ResponseEntity<Drink> deleteAllDrink() {
        try {
            drinkRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
