package com.devcamp.drinkapi.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.drinkapi.models.Drink;

public interface DrinkRepository extends JpaRepository<Drink, Long> {

}
